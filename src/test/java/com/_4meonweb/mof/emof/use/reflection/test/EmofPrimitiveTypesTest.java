package com._4meonweb.mof.emof.use.reflection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import com._4meonweb.mof.emof.use.reflection.BooleanType;
import com._4meonweb.mof.emof.use.reflection.IntegerType;
import com._4meonweb.mof.emof.use.reflection.PrimitiveTypePackage;
import com._4meonweb.mof.emof.use.reflection.StringType;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;
import com._4meonweb.uml.classification.RedefinableElement;
import com._4meonweb.uml.classification.test.ClassifierBond;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.provider.Arguments;

/** Unit test static EMOF Primitive String.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
class EmofPrimitiveTypesTest implements ClassifierBond {

  @Inject
  @Named("Boolean")
  transient PrimitiveType blnTp;

  @Inject
  @Named("Integer")
  transient PrimitiveType intrTp;

  @Inject
  transient UmlStringMaker strMkr;

  @Inject
  @Named("String")
  transient PrimitiveType strTp;

  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(PrimitiveTypePackage.class,
      BooleanType.class, UmlStringMakerUse.class, IntegerType.class,
      StringType.class, PrimitiveTypePackage.class);

  protected EmofPrimitiveTypesTest() {
    super();
  }

  @Override
  public Stream<AccessorWithRelations> getAccessors() {
    // TODO Auto-generated method stub
    return ClassifierBond.super.getAccessors();
  }

  @Override
  public Stream<NamespaceWithAttributes> getNamespaceWithAttributes() {
    return this.getPrimitivesWithAttributes().map(Arguments::get).map(argt -> {
      return new NamespaceWithAttributes() {

        @Override
        public Optional<String> getName() {
          return Optional.ofNullable(argt[1]).map(String.class::cast);
        }

        @Override
        public Namespace getNamespace() {
          return (Namespace) argt[0];
        }

      };
    });
  }

  @TestFactory
  Stream<DynamicTest> getOwnedComments_MofPrimitive_Single() {
    return this.getPrimitivesWithAttributes().map(Arguments::get)
        .map(prmv -> dynamicTest(prmv[0].toString(),
            () -> assertEquals(1L,
                ((Element) prmv[0]).getOwnedComments().count(),
                "Comment is missed or not single")));
  }

  protected Stream<Arguments> getPrimitivesWithAttributes() {
    return Stream.of(
        arguments(this.blnTp, "Boolean",
            "Boolean is used for logical expressions, "
                + "consisting of the predefined values true and false."),
        arguments(this.strTp, "String",
            "String is a sequence of characters in some suitable character set"
                + " used to display information about the model."
                + " Character sets may include non-Roman alphabets and characters."),
        arguments(this.intrTp, "Integer",
            "Integer is a primitive type representing integer values."));
  }

  @Override
  public Stream<RedefinableWithAttributes> getRedefinableElements() {
    return this.getPrimitivesWithAttributes().map(Arguments::get).map(argt -> {
      return new RedefinableWithAttributes() {

        @Override
        public Optional<String> getName() {
          return Optional.ofNullable(argt[1]).map(String.class::cast);
        }

        @Override
        public RedefinableElement getRedefinableElement() {
          return (RedefinableElement) argt[0];
        }

      };
    });
  }

  @Override
  public UmlStringMaker getStringMaker() {
    return this.strMkr;
  }
}
