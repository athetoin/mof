package com._4meonweb.mof.emof.use.reflection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import com._4meonweb.mof.emof.use.reflection.BooleanType;
import com._4meonweb.mof.emof.use.reflection.IntegerType;
import com._4meonweb.mof.emof.use.reflection.PrimitiveTypePackage;
import com._4meonweb.mof.emof.use.reflection.StringType;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.commonstructures.test.NamespaceBond.NamespaceWithAttributes;
import com._4meonweb.uml.commonstructures.test.UmlPackageBond;

import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;

/** Unit test for predefined Primitive Type package imported by MOF.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
class EmofPackagePrimitiveTypesTest implements UmlPackageBond {

  @Inject
  @Named("PrimitiveTypes")
  transient UmlPackage prmvPckg;

  @Inject
  transient UmlStringMaker strMkr;

  @WeldSetup
  public WeldInitiator weld =
      WeldInitiator.of(PrimitiveTypePackage.class, BooleanType.class,
          UmlStringMakerUse.class, IntegerType.class, StringType.class);

  protected EmofPackagePrimitiveTypesTest() {
    super();
  }

  @Test
  void getName_Constant_Match() {
    assertEquals("PrimitiveTypes",
        this.prmvPckg.getName().map(UmlString::getValue).orElse(null),
        "Name does not match OMG XMI document");
  }

  @Override
  public Stream<NamespaceWithAttributes> getNamespaceWithAttributes() {
    return this.getPackageWithAttributes().map(Arguments::get).map(argt -> {
      return new NamespaceWithAttributes() {

        @Override
        public Optional<String> getName() {
          return Optional.ofNullable(argt[1]).map(String.class::cast);
        }

        @Override
        public Namespace getNamespace() {
          return (Namespace) argt[0];
        }

      };
    });
  }

  protected Stream<Arguments> getPackageWithAttributes() {
    return Stream.of(arguments(this.prmvPckg, "PrimitiveTypes"));
  }

  @Override
  public UmlStringMaker getStringMaker() {
    return this.strMkr;
  }

  @Test
  void getUri_Constant_Match() {
    assertEquals("http://www.omg.org/spec/PrimitiveTypes/20161101",
        this.prmvPckg.getUri().map(UmlString::getValue).orElse(null),
        "URI does not match OMG XMI document");
  }
}
