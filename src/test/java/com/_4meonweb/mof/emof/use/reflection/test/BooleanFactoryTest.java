package com._4meonweb.mof.emof.use.reflection.test;

import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.mof.emof.api.uml.values.EmofLiteralBoolean;
import com._4meonweb.mof.emof.use.reflection.BooleanType;
import com._4meonweb.mof.emof.use.reflection.IntegerType;
import com._4meonweb.mof.emof.use.reflection.PrimitiveTypePackage;
import com._4meonweb.mof.emof.use.reflection.PrimitivesFactory;
import com._4meonweb.mof.emof.use.reflection.StringType;
import com._4meonweb.mof.emof.use.uml.values.EmofLiteralBooleanUse.EmofLiteralBooleanMakerUse;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.use.UmlBooleanUse.UmlBooleanMakerUse;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.params.provider.Arguments;

/** EMOF Factory unit testing.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
public class BooleanFactoryTest implements EmofFactoryBond {

  @Inject
  @Named("Boolean")
  transient PrimitiveType blnTp;

  @Inject
  transient PrimitivesFactory fctr;

  @Inject
  transient UmlStringMaker strMkr;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(UmlBooleanMakerUse.class,
      EmofLiteralBooleanMakerUse.class, UmlStringMakerUse.class,
      PrimitivesFactory.class, BooleanType.class, PrimitiveTypePackage.class,
      IntegerType.class, StringType.class);

  /** Default constructor. */
  protected BooleanFactoryTest() {
    super();
  }

  @Override
  public Stream<Arguments> getFactoryTypeStringObject() {
    final var trLtrl = mock(EmofLiteralBoolean.class);
    when(trLtrl.getValue()).thenReturn(UmlBooleanStatic.TRUE);
    final var flsLtrl = mock(EmofLiteralBoolean.class);
    when(flsLtrl.getValue()).thenReturn(UmlBooleanStatic.FALSE);
    return Stream.of(
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("FALSE"), null,
            false),
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("bad value"), null,
            false),
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("true"), trLtrl,
            true),
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("1"), trLtrl, false),
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("false"), flsLtrl,
            true),
        arguments(this.fctr, this.blnTp, this.strMkr.ofThe("0"), flsLtrl,
            false));
  }
}
