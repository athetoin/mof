package com._4meonweb.mof.emof.use.reflection.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.reflection.EmofFactory;
import com._4meonweb.mof.emof.api.uml.simpleclassifiers.EmofDataType;
import com._4meonweb.mof.emof.api.uml.values.LiteralStatic;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.simpleclassifiers.DataType;

import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.provider.Arguments;

public interface EmofFactoryBond {
  /** Creates tests for exception on NULL data type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> convertToString_NullDataType_ThrowNull() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .map(arr -> dynamicTest("<" + ((UmlString) arr[2]).getValue() + ">",
            () -> assertThrows(NullPointerException.class,
                () -> ((EmofFactory) arr[0]).convertToString(null,
                    (EmofObject) arr[3]))));
  }

  /** Creates tests of successful primitive type creation.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> convertToString_Ok_Exists() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .filter(arr -> arr[3] != null)
        .map(arr -> dynamicTest(arr[1] + ": " + ((UmlString) arr[2]).getValue(),
            () -> assertNotNull(((EmofFactory) arr[0])
                .convertToString((DataType) arr[1], (EmofObject) arr[3]),
                () -> "Cannot create string")));
  }

  /** Creates tests of value of created primitive type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> convertToString_Ok_MatchedValue() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .filter(
            arr -> arr[3] != null && (boolean) arr[4])
        .map(arr -> dynamicTest(arr[1] + ": " + ((UmlString) arr[2]).getValue(),
            () -> assertEquals(((UmlString) arr[2]).getValue(),
                ((EmofFactory) arr[0])
                    .convertToString((DataType) arr[1], (EmofObject) arr[3])
                    .getValue(),
                () -> "Unexpected value")));
  }

  /** Creates tests for exception on NULL data type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> convertToString_WrongDataType_ThrowIllegal() {
    return this.getFactoryTypeStringObject().map(Arguments::get).map(
        arr -> dynamicTest("<" + ((UmlString) arr[2]).getValue() + ">", () -> {
          final var m = mock(EmofDataType.class);
          assertThrows(IllegalArgumentException.class,
              () -> ((EmofFactory) arr[0]).convertToString(m,
                  (EmofObject) arr[3]));
        }));
  }

  /** Creates tests of bad string for primitive type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> createFromString_BadString_LiteralNull() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .filter(arr -> arr[3] == null)
        .map(arr -> dynamicTest(arr[1] + ": " + ((UmlString) arr[2]).getValue(),
            () -> assertEquals(LiteralStatic.NULL,
                ((EmofFactory) arr[0]).createFromString((DataType) arr[1],
                    (UmlString) arr[2]),
                () -> "Created primitive from ineligble string: " + arr[2])));
  }

  /** Creates tests for exception on NULL data type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> createFromString_NullDataType_ThrowNull() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .map(arr -> dynamicTest("<" + ((UmlString) arr[2]).getValue() + ">",
            () -> assertThrows(NullPointerException.class,
                () -> ((EmofFactory) arr[0]).createFromString(null,
                    (UmlString) arr[2]))));
  }

  /** Creates tests of successful primitive type creation.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> createFromString_Ok_Exists() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .filter(arr -> arr[3] != null)
        .map(arr -> dynamicTest(arr[1] + ": " + ((UmlString) arr[2]).getValue(),
            () -> assertNotNull(((EmofFactory) arr[0])
                .createFromString((DataType) arr[1], (UmlString) arr[2]),
                () -> "Cannot create primitive")));
  }

  /** Creates tests of value of created primitive type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> createFromString_Ok_MatchedValue() {
    return this.getFactoryTypeStringObject().map(Arguments::get)
        .filter(arr -> arr[3] != null)
        .map(arr -> dynamicTest(arr[1] + ": " + ((UmlString) arr[2]).getValue(),
            () -> assertEquals(((EmofFactory) arr[0])
                .createFromString((DataType) arr[1], (UmlString) arr[2]),
                arr[3], () -> "Unexpected value")));
  }

  /** Creates tests for exception on NULL data type.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> createFromString_WrongDataType_ThrowIllegal() {
    return this.getFactoryTypeStringObject().map(Arguments::get).map(
        arr -> dynamicTest("<" + ((UmlString) arr[2]).getValue() + ">", () -> {
          final var m = mock(EmofDataType.class);
          assertThrows(IllegalArgumentException.class,
              () -> ((EmofFactory) arr[0]).createFromString(m,
                  (UmlString) arr[2]));
        }));
  }

  /** Gets factory arguments for creation/conversion from/to string.
   *
   * @return the arguments */
  Stream<Arguments> getFactoryTypeStringObject();
}
