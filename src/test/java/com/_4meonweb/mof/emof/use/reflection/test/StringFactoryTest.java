package com._4meonweb.mof.emof.use.reflection.test;

import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.mof.emof.api.uml.values.EmofLiteralString;
import com._4meonweb.mof.emof.use.reflection.BooleanType;
import com._4meonweb.mof.emof.use.reflection.IntegerType;
import com._4meonweb.mof.emof.use.reflection.PrimitiveTypePackage;
import com._4meonweb.mof.emof.use.reflection.PrimitivesFactory;
import com._4meonweb.mof.emof.use.reflection.StringType;
import com._4meonweb.mof.emof.use.uml.values.EmofLiteralStringUse.EmofLiteralStringMakerUse;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.use.UmlBooleanUse.UmlBooleanMakerUse;
import com._4meonweb.primitivetypes.use.UmlStringUse;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.params.provider.Arguments;

/** EMOF Factory unit testing.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
class StringFactoryTest implements EmofFactoryBond {

  @Inject
  transient PrimitivesFactory fctr;

  @Inject
  transient UmlStringMaker strMkr;

  @Inject
  @Named("String")
  transient PrimitiveType strTp;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(UmlStringUse.class,
      UmlStringMakerUse.class, UmlBooleanMakerUse.class,
      EmofLiteralStringMakerUse.class, EmofLiteralStringMakerUse.class,
      PrimitivesFactory.class, StringType.class, BooleanType.class,
      IntegerType.class, PrimitiveTypePackage.class);

  /** Default constructor. */
  public StringFactoryTest() {
    super();
  }

  @Override
  public Stream<Arguments> getFactoryTypeStringObject() {
    final Function<String, EmofLiteralString> prpdLiteral = (str) -> {
      final var mck = mock(EmofLiteralString.class);
      when(mck.getValue()).thenReturn(Optional.of(this.strMkr.ofThe(str)));
      return mck;
    };

    final String str = " some string ";

    return Stream.of(
        arguments(this.fctr, this.strTp, this.strMkr.ofThe(str),
            prpdLiteral.apply(str), true),
        arguments(this.fctr, this.strTp, this.strMkr.ofThe(" \u0000\n"),
            prpdLiteral.apply(" \n"), false));
  }
}
