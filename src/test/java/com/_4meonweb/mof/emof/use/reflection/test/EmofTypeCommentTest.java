package com._4meonweb.mof.emof.use.reflection.test;

import com._4meonweb.mof.emof.use.reflection.BooleanType;
import com._4meonweb.mof.emof.use.reflection.IntegerType;
import com._4meonweb.mof.emof.use.reflection.PrimitiveTypePackage;
import com._4meonweb.mof.emof.use.reflection.StringType;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;
import com._4meonweb.uml.commonstructures.Comment;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.commonstructures.test.CommentBond;

import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;

@EnableAutoWeld
public class EmofTypeCommentTest implements CommentBond {

  @Inject
  @Named("PrimitiveTypes")
  transient UmlPackage prmvPckg;

  @Inject
  transient UmlStringMaker strMkr;

  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(PrimitiveTypePackage.class,
      BooleanType.class, UmlStringMakerUse.class, IntegerType.class,
      StringType.class, EmofPrimitiveTypesTest.class);

  @Override
  public Stream<CommentWithAttributes> getCommentsWithAttributes() {
    return this.prmvPckg.getOwnedElements().flatMap(Element::getOwnedComments)
        .map(cmnt -> new CommentWithAttributes() {

          @Override
          public Stream<Element> getAnnotated() {
            return cmnt.getOwner().stream();
          }

          @Override
          public Optional<String> getBody() {
            switch (cmnt.toString()) {
              case "Description of Boolean":
                return Optional.of("Boolean is used for logical expressions, "
                    + "consisting of the predefined values true and false.");
              case "Description of Integer":
                return Optional.of(
                    "Integer is a primitive type representing integer values.");
              case "Description of String":
                return Optional.of(
                    "String is a sequence of characters in some suitable character set"
                        + " used to display information about the model."
                        + " Character sets may include non-Roman alphabets and characters.");
              default:
                return Optional.empty();
            }
          }

          @Override
          public Comment getComment() {
            return cmnt;
          }
        });
  }

  @Override
  public UmlStringMaker getStringMaker() {
    return this.strMkr;
  }
}