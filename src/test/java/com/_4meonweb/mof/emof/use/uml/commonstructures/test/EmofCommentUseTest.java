package com._4meonweb.mof.emof.use.uml.commonstructures.test;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofComment;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofComment.EmofCommentMaker;
import com._4meonweb.mof.emof.api.uml.commonstructures.test.EmofCommentBond;
import com._4meonweb.mof.emof.use.uml.commonstructures.EmofCommentUse;
import com._4meonweb.mof.emof.use.uml.commonstructures.EmofCommentUse.EmofCommentMakerUse;
import com._4meonweb.uml.structuredclassifier.Property;

import java.util.Objects;
import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

@EnableAutoWeld
public class EmofCommentUseTest implements EmofCommentBond {
  transient EmofCommentMaker cmntMkr;

  /** Boolean Maker for tested primitive creation. */
  @Inject
  transient Instance<EmofCommentMaker> cmntMkrFctr;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld =
      WeldInitiator.of(EmofCommentUse.class, EmofCommentMakerUse.class);

  public EmofCommentUseTest() {
    super();
  }

  @Override
  public EmofComment getEmptyTested() {
    final EmofComment t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<EmofComment> getInstancesOfTested() {
    final Stream<EmofComment> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<EmofCommentMaker> getInvalidMakers() {
    final Stream<EmofCommentMaker> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @BeforeEach
  void getMakerInstance() {
    this.cmntMkr = this.cmntMkrFctr.get();
  }

  @Override
  public Stream<EmofObject[]> getPairsWithSameValue() {
    final Stream<EmofObject[]> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<EmofObject[]> getPairWithAnotherDataTypeValue() {
    final Stream<EmofObject[]> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<EmofObject[]> getPairWithAnotherInstanceOrClass() {
    final Stream<EmofObject[]> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<EmofObject[]> getPairWithNonClassOrDataType() {
    final Stream<EmofObject[]> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<Property> getProperties() {
    return Stream.of(Mockito.mock(Property.class));
  }

  @Override
  public Stream<Object[]> getPropertiesWithValues() {
    final Stream<Object[]> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

  @Override
  public Stream<Object[]> getPropertiesWithWrongTypeValue() {
    final Object[] arr = { Mockito.mock(Property.class), null };
    return Stream.ofNullable(arr);
  }

  @Override
  public Stream<EmofCommentMaker> getValidTested() {
    return Stream.of(this.cmntMkr);
  }

  @Override
  public Stream<Property> getWrongProperties() {
    final Stream<Property> t = null;
    assumeTrue(Objects.nonNull(t));
    return t;
  }

}
