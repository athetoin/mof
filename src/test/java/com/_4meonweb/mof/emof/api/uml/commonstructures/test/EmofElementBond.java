package com._4meonweb.mof.emof.api.uml.commonstructures.test;

import com._4meonweb.mof.emof.api.test.EmofObjectBond;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement.EmofElementMaker;

public interface EmofElementBond<E extends EmofElement,
    M extends EmofElementMaker<E, M>> extends EmofObjectBond<E, M> {

}
