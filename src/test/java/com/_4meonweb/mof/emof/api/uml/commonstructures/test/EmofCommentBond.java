package com._4meonweb.mof.emof.api.uml.commonstructures.test;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofComment;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofComment.EmofCommentMaker;

public interface EmofCommentBond
    extends EmofElementBond<EmofComment, EmofCommentMaker> {

}
