package com._4meonweb.mof.emof.api.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.EmofObject.EmofObjectMaker;
import com._4meonweb.mof.emof.api.common.ReflectiveCollection;
import com._4meonweb.uml.structuredclassifier.Property;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

/** EMOF Object unit test template.
 *
 * @author Maxim Rodyushkin */
public interface EmofObjectBond<O extends EmofObject,
    M extends EmofObjectMaker<O, M>> {
  /** Tests equals: Returns false for all other cases. */
  @TestFactory
  default Stream<DynamicTest> equals_NonClassOrDataType_False() {
    return this.getPairWithNonClassOrDataType().map(insts -> dynamicTest(
        "Check <" + insts[0] + "> with <" + insts[1] + ">", () -> {
          assertTrue(
              !insts[0].objectEquals(insts[1]).getValue()
                  && !insts[0].equals(insts[1]),
              "Neither Class nor Data Type is equal");
        }));
  }

  /** Tests equals: Negated case for instances of DataType, returns true if the
   * object has the same value as this Object instance. */
  @TestFactory
  default Stream<DynamicTest> equals_OtherDataTypeValue_False() {
    final String msg = "Different value is equal";
    return this.getPairWithAnotherDataTypeValue()
        .map(insts -> dynamicTest("Check <" + insts[0].getClass()
            + "> with value <" + insts[0] + "> and <" + insts[1].getClass()
            + "> with value <" + insts[1] + ">", () -> {
              assertFalse(insts[0].objectEquals(insts[1]).getValue(), msg);
              assertNotEquals(insts[0], insts[1], msg);
            }));
  }

  /** Tests equals: Negated case for instances of Class, returns true if the
   * object and this Object instance are references to the same Object. */
  @TestFactory
  default Stream<DynamicTest> equals_OtherInstanceOrClass_False() {
    final String msg = "Different instance or Class is equal";
    assumeTrue(Objects.nonNull(this.getPairWithAnotherInstanceOrClass()));
    return this.getPairWithAnotherInstanceOrClass()
        .map(insts -> dynamicTest("Check <" + insts[0] + "> with <"
            + insts[1].getClass() + "> instance <" + insts[1] + ">", () -> {
              assertFalse(insts[0].objectEquals(insts[1]).getValue(), msg);
              assertNotEquals(insts[0], insts[1], msg);
            }));
  }

  /** Tests equals: For instances of Class, returns true if the object and this
   * Object instance are references to the same Object. */
  @TestFactory
  default Stream<DynamicTest> equals_SameInstanceClass_True() {
    return this.getInstancesOfTested()
        .map(inst -> dynamicTest("Self equality of <" + inst + ">", () -> {
          assertTrue(inst.objectEquals(inst).getValue() && inst.equals(inst),
              "Same class instances are not equal");
        }));
  }

  /** Tests equals: For instances of DataType, returns true if the object has
   * the same value as this Object instance. */
  @TestFactory
  default Stream<DynamicTest> equals_SameValue_True() {
    return this.getPairsWithSameValue()
        .map(insts -> dynamicTest("Check <" + insts[0].getClass()
            + "> with value <" + insts[0] + "> and <" + insts[1].getClass()
            + "> with value <" + insts[1] + ">", () -> {
              assertTrue(
                  insts[0].objectEquals(insts[1]).getValue()
                      && insts[0].equals(insts[1]),
                  "Data types with the same value are not equal");
            }));
  }

  /** Tests get: throws IllegalArgumentException if Property is not a member of
   * the Class from class(). */
  @TestFactory
  default Stream<DynamicTest> get_WrongProperty_ThrowArgument() {
    final var tstd = this.getEmptyTested();
    return this.getWrongProperties()
        .map(prpt -> dynamicTest("Check <" + prpt + ">",
            () -> assertThrows(IllegalArgumentException.class,
                () -> tstd.get(prpt),
                "Wrong property does not throw argument exception")));
  }

  /** Get tested object.
   *
   * @return the object */
  O getEmptyTested();

  /** Gets different instances of tested object.
   *
   * @return the instances */
  Stream<O> getInstancesOfTested();

  /** Gets makers with violated constraints.
   *
   * @return the makers */
  Stream<M> getInvalidMakers();

  /** Tests initial default value for property with multiplicity 1. */
  @TestFactory
  default Stream<DynamicTest> getIsSet_Mlt1AndInitial_NotSetAndDefault() {
    final var tstd = this.getEmptyTested();
    return this.getProperties().peek(prpt -> {
      final var t0 = prpt;
      assumeTrue(Objects.nonNull(t0.getUpper()));
    }).filter(prpt -> prpt.getUpper().getValue() == 1)
        .map(prpt -> dynamicTest("Check <" + prpt + ">", () -> {
          assertEquals(prpt.getDefaultValue(), tstd.get(prpt),
              "Just created  value is not default");
          assertFalse(tstd.isSet(prpt).getValue(), "Initial value is set");
        }));
  }

  /** Tests initial empty collection for property with multiplicity more than
   * 1. */
  @TestFactory
  default Stream<DynamicTest> getIsSet_MltMore1AndInitial_NotSetAndEmpty() {
    final var tstd = this.getEmptyTested();
    return this.getProperties().peek(prpt -> {
      final var t0 = prpt;
      assumeTrue(Objects.nonNull(t0.getUpper()));
    }).filter(prpt -> prpt.getUpper().getValue() > 1)
        .map(prpt -> dynamicTest("Check <" + prpt + ">", () -> {
          assertEquals(Optional.of(0L),
              tstd.get(prpt).filter(Supplier.class::isInstance)
                  .map(Supplier.class::cast).map(Supplier::get)
                  .filter(Stream.class::isInstance).map(Stream.class::cast)
                  .map(Stream::count),
              "Just created property with multiple values is not empty");
          assertFalse(tstd.isSet(prpt).getValue(), "Initial value is set");
        }));
  }

  /** Gets same data type value pairs.
   *
   * @return the value pairs */
  Stream<EmofObject[]> getPairsWithSameValue();

  /** Gets pairs of different Data type values.
   *
   * @return the value pairs */
  Stream<EmofObject[]> getPairWithAnotherDataTypeValue();

  /** Gets pair of class instances with mismatched class or instances.
   *
   * @return the value pairs */
  Stream<EmofObject[]> getPairWithAnotherInstanceOrClass();

  /** Gets value of pairs where at least one value is not Class or Data Type.
   *
   * @return the value pairs */
  Stream<EmofObject[]> getPairWithNonClassOrDataType();

  /** Get defined properties.
   *
   * @return the properties */
  Stream<Property> getProperties();

  /** Gets properties with valid values.
   *
   * @return the properties and values */
  Stream<Object[]> getPropertiesWithValues();

  /** Gets properties paired with wrong type value.
   *
   * @return the pairs */
  Stream<Object[]> getPropertiesWithWrongTypeValue();

  /** Get validated makers ready for immutable object creation.
   *
   * @return the makers */
  Stream<M> getValidTested();

  /** Get Property that does not belong tested object.
   *
   * @return the property */
  Stream<Property> getWrongProperties();

  /** Tests Exception: throws IllegalArgumentException if Property is not a
   * member of the Class from getMetaClass (). */
  @TestFactory
  default Stream<DynamicTest> isSet_WrongProperty_ThrowArgument() {
    final var tstd = this.getEmptyTested();
    return this.getWrongProperties()
        .map(prpt -> dynamicTest("Check <" + prpt + ">", () -> {
          assertThrows(IllegalArgumentException.class, () -> tstd.isSet(prpt),
              "Wrong property does not throw argument exception");
        }));
  }

  /** Tests maker with violated constraints that will not return immutable
   * object. */
  @TestFactory
  default Stream<DynamicTest> make_Invalid_Null() {
    return this.getInvalidMakers()
        .map(mkr -> dynamicTest("Check <" + mkr + ">", () -> {
          final var obj = mkr.make();
          assertTrue(obj.isEmpty(), "Invalid maker creates immutable object");
          assertEquals(0, mkr.getViolations0().size(),
              "No violations for invalid maker");
        }));
  }

  /** Tests successful creation of immutable form. */
  @TestFactory
  default Stream<DynamicTest> make_Valid_Immutable() {
    return this.getValidTested()
        .map(mkr -> dynamicTest("Check <" + mkr + ">", () -> {
          final var obj = mkr.make();
          assumeTrue(obj.isPresent());
          assertTrue(obj.isPresent(),
              "Valid maker cannot create immutable object");
          assertEquals(0, mkr.getViolations0().size(),
              "Returned violations after immutable object creation");
        }));
  }

  /** Tests set Exception: throws ClassCastException if the Property’s type
   * isInstance(object) returns false and Property has multiplicity upper bound
   * = 1. */
  @TestFactory
  default Stream<DynamicTest> set_Mlt1AndObjectNotPropertyType_ThrowCast() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    return this.getPropertiesWithWrongTypeValue().peek(prpt -> {
      final var t0 = (Property) prpt[0];
      assumeTrue(Objects.nonNull(t0.getUpper()));
    }).filter(prpt -> ((Property) prpt[0]).getUpper().getValue() == 1)
        .map(prpt -> dynamicTest("Check <" + prpt[0] + "> with value type <"
            + prpt[1].getClass() + ">", () -> {
              assertThrows(ClassCastException.class,
                  () -> tstd.set((Property) prpt[0], (EmofObject) prpt[1]),
                  "No exception on wrong type for property");
            }));
  }

  /** Tests set Exception: throws IllegalArgumentException if element is null,
   * Property is of type Class, and the multiplicity upper bound > 1. */
  @TestFactory
  default Stream<
      DynamicTest> set_MltMore1AndClassAndNullObject_ThrowArgument() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    return this.getProperties().peek(prpt -> {
      final var t0 = prpt;
      assumeTrue(Objects.nonNull(t0.getUpper()));
    }).filter(prpt -> prpt.getUpper().getValue() > 1)
        .map(prpt -> dynamicTest("Check <" + prpt + ">", () -> {
          assertThrows(IllegalArgumentException.class,
              () -> tstd.set(prpt, null),
              "No exception on null value for single-valued property");
        }));
  }

  /** Tests set Exception: throws ClassCastException if Element is not a
   * ReflectiveCollection and Property has multiplicity upper bound > 1. */
  @TestFactory
  default Stream<DynamicTest> set_MltMore1AndNotCollection_ThrowCast() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    return this.getPropertiesWithWrongTypeValue().peek(prpt -> {
      final var t0 = (Property) prpt[0];
      assumeTrue(Objects.nonNull(t0.getUpper()));
    }).filter(prpt -> ((Property) prpt[0]).getUpper().getValue() > 1
        && !ReflectiveCollection.class.isInstance(prpt[1]))
        .map(prpt -> dynamicTest("Check <" + prpt[0] + "> with value type <"
            + prpt[1].getClass() + ">", () -> {
              assertThrows(ClassCastException.class,
                  () -> tstd.set((Property) prpt[0], (EmofObject) prpt[1]),
                  "No exception on multivalued-property without collection");
            }));
  }

  /** Tests set Exception: throws IllegalArgumentException if Property is not a
   * member of the Class from getMetaClass(). */
  @TestFactory
  default Stream<DynamicTest> set_WrongProperty_ThrowArgument() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    assumeTrue(Objects.nonNull(this.getWrongProperties()));
    return this.getWrongProperties()
        .map(prpt -> dynamicTest("Check <" + prpt + ">", () -> {
          assertThrows(IllegalArgumentException.class,
              () -> tstd.set(prpt, null),
              "Wrong property does not throw argument exception");
        }));
  }

  /** Tests set and get get for property with multiplicity 1.
   *
   * <p>For get(), if the Property has multiplicity upper bound of 1, get()
   * returns the value of the Property.
   *
   * <p>For set(). If the Property has multiplicity upper bound = 1, set()
   * atomically updates the value of the Property to the object parameter. */
  @TestFactory
  default Stream<DynamicTest> setGet_Mlt1_Value() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    return this.getPropertiesWithValues()
        .filter(prpt -> ((Property) prpt[0]).getUpper().getValue() == 1)
        .map(prpt -> dynamicTest(
            "Check <" + prpt[0] + "> with value <" + prpt[1] + ">", () -> {
              final var chgd =
                  tstd.set((Property) prpt[0], (EmofObject) prpt[1]);
              assertEquals(tstd, chgd, "Set did not return the same object");
              assertEquals(prpt[1], chgd.get((Property) prpt[0]),
                  "Get did not return setted value");
            }));
  }

  /** Tests set and get for property with multiplicity more than 1 and
   * non-empty.
   *
   * <p>If Property has multiplicity upper bound more than 1, get() returns a
   * ReflectiveCollection containing the values of the Property.
   *
   * <p>For set(), if Property has multiplicity upper bound more than 1, the
   * Object must be a kind of ReflectiveCollection. The behavior is identical to
   * the following operations performed atomically:
   *
   * <pre>
   * ReflectiveSequence list = element.get(property);
   * list.clear();
   * list.addAll((ReflectiveSequence) object);
   * </pre>
   */
  @TestFactory
  default Stream<DynamicTest> setGet_MltMore1_Collection() {
    final var tstd = this.getValidTested().findFirst().orElse(null);
    return this.getPropertiesWithValues()
        .filter(prpt -> ((Property) prpt[0]).getUpper().getValue() > 1)
        .map(prpt -> dynamicTest(
            "Check <" + prpt[0] + "> with value <" + prpt[1] + ">", () -> {
              final var chgd =
                  tstd.set((Property) prpt[0], (EmofObject) prpt[1]);
              assertEquals(tstd, chgd, "Set did not return the same object");
            }));
  }

  /** Tests set and get for property with multiplicity more than 1 and empty
   * collection.
   *
   * <p>If there are no values, the ReflectiveCollection returned is empty. */
  @Test
  default void setGet_MltMore1NoValues_EmptyCollection() {
  }

  /** Tests isSet after set: Negated case for, if the Property has multiplicity
   * upper bound of 1, isSet() returns true if the value of the Property is
   * different than the default value of that property. */
  @Test
  default void setIsSet_Mlt1AndDefault_False() {
  }

  /** Tests isSet after set: If the Property has multiplicity upper bound of 1,
   * isSet() returns true if the value of the Property is different than the
   * default value of that property. */
  @Test
  default void setIsSet_Mlt1AndNonDefault_True() {
  }

  /** Tests isSet after set: Negated case for, if Property has multiplicity
   * upper bound >1, isSet() returns true if the number of objects in the list
   * is > 0. */
  @Test
  default void setIsSet_MltMore1AndEmptyCollection_False() {
  }

  /** Tests isSet after set: If Property has multiplicity upper bound >1,
   * isSet() returns true if the number of objects in the list is > 0. */
  @Test
  default void setIsSet_MltMore1AndNonEmptyCollection_True() {
  }

  /** Tests unset: If the Property has multiplicity upper bound of 1, unset()
   * atomically sets the value of the Property to its default value for DataType
   * type properties and null for Class type properties. */
  @Test
  default void setUnsetGetIsSet_Mlt1_DefaultAndTrue() {
  }

  /** Tests unset: If Property has multiplicity upper bound >1, unset() clears
   * the ReflectiveCollection of values of the Property. The behavior is
   * identical to the following operations performed atomically:
   *
   * <pre>
  *    ReflectiveCollection list = object.get(property);
  *    list.clear()
   * </pre>
   */
  @Test
  default void setUnsetGetIsSet_MltMore1_EmptyCollectionAndTrue() {
  }

  /** Tests unset Exception: throws IllegalArgumentException if Property is not
   * a member of the Class from getMetaClass(). */
  @Test
  default void unset_WrongProperty_ThrowArgument() {
  }

  /** Tests unset makes no changes in initial state of property with
   * multiplicity 1. */
  @Test
  default void unsetGetIsSet_Mlt1_DefaultAndFalse() {
  }

  /** Tests unset makes no changes in initial state of property with
   * multiplicity more than 1. */
  @Test
  default void unsetGetIsSet_MltMore1_EmptyCollectionAndFalse() {
  }
}
