package com._4meonweb.mof.emof.api.uml.classification;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofNamedElement;
import com._4meonweb.uml.classification.RedefinableElement;

/** Resulting EMOF Redefinable Element merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofRedefinableElement
    extends RedefinableElement, EmofNamedElement {

}
