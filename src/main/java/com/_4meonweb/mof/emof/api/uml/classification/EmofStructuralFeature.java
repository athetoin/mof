package com._4meonweb.mof.emof.api.uml.classification;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofMultiplicityElement;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofTypedElement;
import com._4meonweb.uml.classification.StructuralFeature;

/** Resulting EMOF Structural Feature merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofStructuralFeature extends StructuralFeature,
    EmofMultiplicityElement, EmofTypedElement, EmofFeature {

}
