/** Resulting package of EMOF merging to MOF Reflection package.
 *
 * <p>Some resulting interfaces supporting used by MOF Reflection package have
 * been moved in this package to avoid package revference's cycles.
 *
 * @author Maxim Rodyushkin */
package com._4meonweb.mof.emof.api.reflection;