package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.Comment;
import com._4meonweb.uml.commonstructures.Element;

import java.util.stream.Stream;

/** Resulting EMOF Comment merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofComment extends Comment, EmofElement {
  interface EmofCommentMaker
      extends EmofElementMaker<EmofComment, EmofCommentMaker> {
    /** Puts associated annotated elements.
     *
     * @param elements
     *          the elements
     * @return this builder */
    default EmofCommentMaker putAnnotatedElements(
        final Stream<Element> elements) {
      return this.getItself();
    }

    /** Puts comment's body.
     *
     * @param body
     *          the comment's body
     * @return this builder */
    default EmofCommentMaker putBody(final UmlString body) {
      return this.getItself();
    }
  }
}
