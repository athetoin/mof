package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.uml.commonstructures.PackageableElement;

/** Resulting EMOF Packageable Element merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofPackageableElement
    extends PackageableElement, EmofNamedElement {

  /** EMOF Packageable Element builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>
   *          the implemented element type
   * @param <M>
   *          the implemented builder type */
  public interface EmofPackageableElementMaker<E extends EmofPackageableElement,
      M extends EmofPackageableElementMaker<E, M>>
      extends /* PackageableElementMaker<E, M>, */ EmofNamedElementMaker<E, M> {

  }
}
