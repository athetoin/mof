package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.uml.commonstructures.UmlType;

/** Resulting EMF Type merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofType extends UmlType, EmofPackageableElement {

  public interface EmofTypeMaker<E extends EmofType,
      M extends EmofTypeMaker<E, M>> {

  }

  /** Compares own and provided obect's types.
   *
   * @param object
   *          the provided object
   * @return is matched type. Default is FALSE */
  default UmlBoolean isInstance(final EmofObject object) {
    return this.conformsTo(((EmofTypedElement) object).getType().orElse(null));
  }
}
