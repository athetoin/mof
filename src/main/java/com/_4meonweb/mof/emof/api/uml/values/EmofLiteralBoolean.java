package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.uml.values.LiteralBoolean;

import java.util.NoSuchElementException;

public interface EmofLiteralBoolean
    extends EmofLiteralSpecification, LiteralBoolean {

  public interface EmofLiteralBooleanMaker
      extends EmofLiteralBoolean, EmofLiteralSpecificationMaker<
          EmofLiteralBoolean, EmofLiteralBooleanMaker> {

    default EmofLiteralBooleanMaker putValue(final UmlBoolean value) {
      this.set(LiteralBooleanProperties.VALUE, this.ofThe(value));
      return this.getItself();
    }
  }

  @Override
  default UmlBoolean getValue() {
    return this.get(LiteralBooleanProperties.VALUE).map(EmofObject::getObject)
        .map(UmlBoolean.class::cast)
        .orElseThrow(() -> new NoSuchElementException("No value"));
  }
}
