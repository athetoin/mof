package com._4meonweb.mof.emof.api.uml.structuredclassifier;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofTypedElement;
import com._4meonweb.uml.structuredclassifier.ConnectableElement;

/** Resulting EMOF Connectable Element merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofConnectableElement
    extends ConnectableElement, EmofTypedElement {

}
