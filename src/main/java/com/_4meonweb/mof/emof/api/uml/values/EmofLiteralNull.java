package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.uml.values.LiteralNull;

public interface EmofLiteralNull extends EmofLiteralSpecification, LiteralNull {

}
