package com._4meonweb.mof.emof.api.identifiers;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement;
import com._4meonweb.primitivetypes.UmlString;

public interface UriExtent extends Extent {
  UmlString getContextUri();

  EmofElement getElement(UmlString uri);

  UmlString getUri(EmofElement object);
}
