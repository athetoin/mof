package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.uml.structuredclassifier.Property;

public class LiteralBooleanProperties {

  public static final Property VALUE = new Property() {
  };

  private LiteralBooleanProperties() {
    super();
  }
}
