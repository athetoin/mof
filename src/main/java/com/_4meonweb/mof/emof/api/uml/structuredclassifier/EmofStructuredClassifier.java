package com._4meonweb.mof.emof.api.uml.structuredclassifier;

import com._4meonweb.mof.emof.api.uml.classifier.EmofClassifier;
import com._4meonweb.uml.structuredclassifier.StructuredClassifier;

/** Resulting EMOF Structured Classifier merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofStructuredClassifier
    extends StructuredClassifier, EmofClassifier {

}
