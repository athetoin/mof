package com._4meonweb.mof.emof.api.reflection;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement;
import com._4meonweb.mof.emof.api.uml.packages.EmofPackage;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralSpecification;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.simpleclassifiers.DataType;
import com._4meonweb.uml.structuredclassifier.UmlClass;

import java.util.Optional;

/** An Element may be created from a Factory. A Factory is an instance of the
 * MOF Factory class. A Factory creates instances of the types in a Package.
 *
 * @author Maxim Rodyushkin */
public interface EmofFactory extends EmofElement {

  /** MOF factory builder.
   *
   * @author Maxim Rodyushkin */
  public interface FactoryMaker extends EmofFactory {
    /** Gets factory instance.
     *
     * @return the factory */
    EmofFactory build();

    @Override
    default UmlString convertToString(final DataType dataType,
        final EmofObject object) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    default Optional<EmofElement> create(final UmlClass metaClass) {
      // TODO Auto-generated method stub
      return Optional.empty();
    }

    @Override
    default EmofLiteralSpecification createFromString(final DataType dataType,
        final UmlString basicString) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    default EmofPackage getPackage() {
      // TODO Auto-generated method stub
      return null;
    }

    /** Sets package owning meta types.
     *
     * @return this builder */
    FactoryMaker putPackage();
  }

  /** Creates a String representation of the object.
   *
   * @param dataType
   *          UML Data Type
   * @param object
   *          MOF instantiated object
   * @return The textual data type value. The format of the String is defined by
   *         the XML Schema SimpleType corresponding to that dataType.
   * @throws IllegalArgumentException
   *           if datatype is not a member of the package returned by
   *           getPackage() or the supplied object is not a valid instance of
   *           that datatype. */
  UmlString convertToString(DataType dataType, EmofObject object);

  /** Creates an element that is an instance of the metaClass.
   *
   * <p>Object::metaClass == metaClass and metaClass.isInstance(object) == true.
   * All properties of the element are considered unset. The values are the same
   * as if object.unset(property) was invoked for every property.
   *
   * @param metaClass
   *          UML Class
   *
   * @return MOF element, or null if the creation cannot be performed. Classes
   *         with abstract = true always return null. The created element’s
   *         metaClass == metaClass.
   *
   * @throws NullPointerException
   *           if class is null.
   * @throws IllegalArgumentException
   *           if class is not a member of the package returned by
   *           getPackage(). */
  Optional<EmofElement> create(UmlClass metaClass);

  /** Initializes value of data type from textual representation.
   *
   * @param dataType
   *          UML data type
   * @param basicString
   *          textual value representation. The format of the String is defined
   *          by the XML Schema SimpleType corresponding to that datatype.
   * @return the value specification
   * @throws NullPointerException
   *           if datatype is null.
   * @throws IllegalArgumentException
   *           if datatype is not a member of the package returned by
   *           getPackage(). */
  EmofLiteralSpecification createFromString(DataType dataType,
      UmlString basicString);

  @Override
  default Optional<Element> getOwner() {
    return Optional.empty();
  }

  /** Returns the package this is a factory for.
   *
   * @return UML Package */
  UmlPackage getPackage();
}
