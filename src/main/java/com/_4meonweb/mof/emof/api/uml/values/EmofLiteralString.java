package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.values.LiteralString;

import java.util.Optional;

/** Resulting EMOF LiteralString merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofLiteralString
    extends LiteralString, EmofLiteralSpecification {

  /** EMOF Literal String builder definition.
   *
   * @author Maxim Rodyushkin */
  public interface EmofLiteralStringMaker extends
      EmofLiteralSpecificationMaker<EmofLiteralString, EmofLiteralStringMaker>,
      EmofLiteralString {

    default EmofLiteralStringMaker putValue(final UmlString value) {
      this.set(LiteralStringProperties.VALUE, this.ofThe(value));
      return this.getItself();
    }
  }

  @Override
  default Optional<UmlString> getValue() {
    return this.get(LiteralStringProperties.VALUE).map(EmofObject::getObject)
        .map(UmlString.class::cast);
  }
}
