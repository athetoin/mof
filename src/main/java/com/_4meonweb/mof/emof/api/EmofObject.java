package com._4meonweb.mof.emof.api;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.uml.structuredclassifier.Property;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;

/** Reflection introduces Object as a supertype of Element in order to be able
 * to have a Type that represents both elements and data values.
 *
 * <p>EmofObject was moved to common package due references between Object and
 * Collections in MOF documentation.
 *
 * @author Maxim Rodyushkin */
public interface EmofObject {
  /** EMOF object builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>
   *          the object type
   * @param <M>
   *          the builder type */
  public interface EmofObjectMaker<E extends EmofObject,
      M extends EmofObjectMaker<E, M>> extends EmofObject {
    /** Gets own instance.
     *
     * @return own instance */
    M getItself();

    /** Gets violation set.
     *
     * <p>It is populated during attempt to create immutable object, if maker
     * violate some constraint.
     *
     * @return the constraint violations */
    default Set<ConstraintViolation<E>> getViolations0() {
      return Collections.emptySet();
    }

    /** Performs transition from mutable to immutable state.
     *
     * @return the immutable object, if properties do not violate
     *         constraints. */
    default Optional<E> make() {
      return Optional.empty();
    }

    /** Creates EMOF Object for any Java class.
     *
     * @param obj
     *          the Java object
     * @return the EMOF Object */
    EmofObject ofThe(Object obj);

    /** Sets property value.
     *
     * <p>If the Property has multiplicity upper bound = 1, set() atomically
     * updates the value of the Property to the object parameter. If Property
     * has multiplicity upper bound >1, the Object must be a kind of
     * ReflectiveCollection. The behavior is identical to the following
     * operations performed atomically:
     *
     * <pre>
     * ReflectiveSequence list = element.get(property);
     * list.clear();
     * list.addAll((ReflectiveSequence) object);
     * </pre>
     *
     * <p>There is no return value.
     *
     * @throw IllegalArgumentException if Property is not a member of the Class
     *        from getMetaClass().
     * @throw ClassCastException if the Property’s type isInstance(object)
     *        returns false and Property has multiplicity upper bound = 1.
     * @throw ClassCastException if Element is not a ReflectiveCollection and
     *        Property has multiplicity upper bound > 1.
     * @throw IllegalArgumentException if element is null, Property is of type
     *        Class, and the multiplicity upper bound > 1.
     *
     * @param property
     *          property specification
     * @param object
     *          property value */
    default M set(final Property property, final EmofObject object) {
      throw new IllegalArgumentException(
          "MOF: Property is not a member of the Class");
    }

    /** Removes property value.
     *
     * <p>If the Property has multiplicity upper bound of 1, unset() atomically
     * sets the value of the Property to its default value for DataType type
     * properties and null for Class type properties. If Property has
     * multiplicity upper bound >1, unset() clears the ReflectiveCollection of
     * values of the Property. The behavior is identical to the following
     * operations performed atomically:
     *
     * <pre>
     * ReflectiveCollection list = object.get(property);
     * list.clear();
     * </pre>
     *
     * <p>There is no return value. After unset() is called,
     * {@code object.isSet(property)} == false.
     *
     * @throw IllegalArgumentException if Property is not a member of the Class
     *        from getMetaClass().
     *
     * @param property
     *          property specification */
    default M unset(final Property property) {
      throw new IllegalArgumentException(
          "MOF: Property is not a member of the Class");
    }
  }

  /** Gets the value of the given property.
   *
   * @param property
   *          property specification
   *
   * @return If the Property has multiplicity upper bound of 1, get() returns
   *         the value of the Property. If Property has multiplicity upper bound
   *         >1, get() returns a ReflectiveCollection containing the values of
   *         the Property. If there are no values, the ReflectiveCollection
   *         returned is empty.
   *
   * @throw IllegalArgumentException if Property is not a member of the Class
   *        from class(). */
  default Optional<EmofObject> get(final Property property) {
    throw new IllegalArgumentException(
        "MOF: Property is not a member of the Class");
  }

  /** Gets Java object if instance created with ofThe, or returns itself.
   *
   * @return the object */
  default Object getObject() {
    return this;
  }

  /** Checks property value.
   *
   * @throw IllegalArgumentException if Property is not a member of the Class
   *        from getMetaClass ().
   *
   * @param property
   *          property specification
   * @return If the Property has multiplicity upper bound of 1, isSet() returns
   *         true if the value of the Property is different than the default
   *         value of that property. If Property has multiplicity upper bound
   *         >1, isSet() returns true if the number of objects in the list is >
   *         0. */
  default UmlBoolean isSet(final Property property) {
    return UmlBooleanStatic.FALSE;
  }

  /** Determines if the object equals this Object instance.
   *
   * @param object
   *          compared object
   * @return For instances of Class, returns true if the object and this Object
   *         instance are references to the same Object. For instances of
   *         DataType, returns true if the object has the same value as this
   *         Object instance. Returns false for all other cases. */
  default UmlBoolean objectEquals(final EmofObject object) {
    return UmlBooleanStatic.FALSE;
  }
}
