package com._4meonweb.mof.emof.api.uml.structuredclassifier;

import com._4meonweb.uml.structuredclassifier.EncapsulatedClassifier;

/** Resulting EMOF Encapsulated Classifiers merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofEncapsulatedClassifier
    extends EncapsulatedClassifier, EmofStructuredClassifier {

}
