package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.structuredclassifier.UmlClass;

import java.util.Optional;
import java.util.stream.Stream;

/** Element merges and extends UML::Element. All model elements that specialize
 * Reflection::Element inherit reflective capabilities.
 *
 * <p>Operations related to properties returned to EMOF Element from EMOF
 * Object, because object has no Properties anyway.
 *
 * @author Maxim Rodyushkin */
public interface EmofElement extends Element, EmofObject {

  /** The EMOF Element builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>the
   *          implemented element type
   * @param <M>the
   *          implemented builder type */
  public interface EmofElementMaker<E extends EmofElement,
      M extends EmofElementMaker<E, M>> extends EmofObjectMaker<E, M> {

    /** Puts comment builders.
     *
     * @param comments
     *          the comments
     * @return this builder */
    default M putOwnedComments(final Stream<EmofComment> comments) {
      return this.getItself();
    }
  }

  /** Returns the parent container of this element if any.
   *
   * @return the container */
  default <E extends EmofElement> Optional<E> getContainer() {
    return Optional.empty();
  }

  /** Returns the Class that describes this element.
   *
   * <p>Every Element has a Class that describes its properties and operations.
   * The Element is an Instance of this Class.
   *
   * <p>This is a derived property provided for convenience and consistency.
   *
   * @return the UML Class */
  default UmlClass getMetaClass() {
    return null;
  }
}
