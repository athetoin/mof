package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.uml.commonstructures.Namespace;

/** Resulting EMOF Namespace merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofNamespace extends Namespace, EmofNamedElement {
  public interface EmofNamespaceMaker {

  }
}
