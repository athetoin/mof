package com._4meonweb.mof.emof.api.extension;

import com._4meonweb.mof.emof.api.common.ReflectiveCollection;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement;
import com._4meonweb.primitivetypes.UmlString;

import java.util.Optional;

public interface ExtTag extends EmofElement {
  <E extends EmofElement> ReflectiveCollection<E> getElement();

  Optional<UmlString> getName();

  UmlString getValue();
}
