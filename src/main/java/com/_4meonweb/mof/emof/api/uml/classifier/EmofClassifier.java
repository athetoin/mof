package com._4meonweb.mof.emof.api.uml.classifier;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofType;
import com._4meonweb.uml.classification.Classifier;

/** Resulting EMOF Classifier merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofClassifier extends Classifier, EmofType {
  public interface EmofClassifierMaker<E extends EmofClassifier,
      M extends EmofClassifierMaker<E, M>> {

  }
}
