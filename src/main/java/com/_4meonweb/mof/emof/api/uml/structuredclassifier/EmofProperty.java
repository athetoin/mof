package com._4meonweb.mof.emof.api.uml.structuredclassifier;

import com._4meonweb.mof.emof.api.uml.classification.EmofStructuralFeature;
import com._4meonweb.mof.emof.api.uml.deployments.EmofDeploymentTarget;
import com._4meonweb.uml.structuredclassifier.Property;

/** Merged UML Property in MOF Reflection. */
public interface EmofProperty extends Property, EmofConnectableElement,
    EmofDeploymentTarget, EmofStructuralFeature {

}
