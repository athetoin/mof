package com._4meonweb.mof.emof.api.uml.deployments;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofPackageableElement;
import com._4meonweb.uml.deployments.InstanceSpecification;

/** Resulting EMOF Instance Specification merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofInstanceSpecification extends InstanceSpecification,
    EmofDeploymentTarget, EmofPackageableElement, EmofDeployedArtifact {

}
