package com._4meonweb.mof.emof.api.uml.packages;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofNamespace;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofPackageableElement;
import com._4meonweb.uml.commonstructures.UmlPackage;

/** Resulting EMOF Package merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofPackage
    extends UmlPackage, EmofPackageableElement, EmofNamespace {
  /** EMOF Package builder.
   *
   * @author Maxim Rodyushkin */
  public interface EmofPackageMaker extends /* UmlPackageMaker<EmofPackage,
                                             * EmofPackageMaker>, */
      EmofPackageableElementMaker<EmofPackage, EmofPackageMaker>,
      EmofNamespaceMaker {

  }
}
