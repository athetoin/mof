package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.uml.commonstructures.Element;

import java.util.Optional;

public enum LiteralStatic implements EmofLiteralNull {
  NULL;

  @Override
  public Object getObject() {
    return this;
  }

  @Override
  public Optional<Element> getOwner() {
    // TODO Auto-generated method stub
    return Optional.empty();
  }
}
