package com._4meonweb.mof.emof.api.identifiers;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.common.ReflectiveSequence;
import com._4meonweb.primitivetypes.UmlBoolean;

public interface Extent extends EmofObject {
  ReflectiveSequence<EmofObject> getElements();

  // TODO define
  UmlBoolean useContainment();
}
