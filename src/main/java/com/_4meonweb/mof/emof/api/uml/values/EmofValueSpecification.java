package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofPackageableElement;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofTypedElement;
import com._4meonweb.uml.values.ValueSpecification;

/** Resulting EMOF Value Specification merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofValueSpecification
    extends ValueSpecification, EmofPackageableElement, EmofTypedElement {

  /** EMOF Specification builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>the
   *          implemented specification type
   * @param <M>the
   *          implemented builder type */
  public interface EmofValueSpecificationMaker<E extends EmofValueSpecification,
      M extends EmofValueSpecificationMaker<E, M>>
      extends EmofPackageableElementMaker<E, M>, EmofTypedElementMaker<E, M> {

  }
}
