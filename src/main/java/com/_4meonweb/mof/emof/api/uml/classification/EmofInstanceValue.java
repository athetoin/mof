package com._4meonweb.mof.emof.api.uml.classification;

import com._4meonweb.mof.emof.api.uml.values.EmofValueSpecification;
import com._4meonweb.uml.classification.InstanceValue;

public interface EmofInstanceValue
    extends InstanceValue, EmofValueSpecification {

}
