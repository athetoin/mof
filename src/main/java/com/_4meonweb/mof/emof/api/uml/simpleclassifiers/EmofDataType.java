package com._4meonweb.mof.emof.api.uml.simpleclassifiers;

import com._4meonweb.mof.emof.api.uml.classifier.EmofClassifier;
import com._4meonweb.uml.simpleclassifiers.DataType;

/** Resulting EMOF Data Type merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofDataType extends DataType, EmofClassifier {

  public interface EmofDataTypeMaker<E extends EmofDataType,
      M extends EmofDataTypeMaker<E, M>> {

  }
}
