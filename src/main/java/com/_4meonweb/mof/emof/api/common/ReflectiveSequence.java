package com._4meonweb.mof.emof.api.common;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.primitivetypes.UmlInteger;

public interface ReflectiveSequence<O extends EmofObject>
    extends ReflectiveCollection<O> {
  void add(UmlInteger index, O object);

  EmofObject get(UmlInteger index);

  void remove(UmlInteger index);

  EmofObject set(UmlInteger index, O object);
}
