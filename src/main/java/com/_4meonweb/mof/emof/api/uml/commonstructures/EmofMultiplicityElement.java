package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.uml.commonstructures.MultiplicityElement;

/** Resulting EMOF Multiplicity Element merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofMultiplicityElement
    extends MultiplicityElement, EmofElement {

}
