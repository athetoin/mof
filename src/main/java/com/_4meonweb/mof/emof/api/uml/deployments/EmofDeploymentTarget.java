package com._4meonweb.mof.emof.api.uml.deployments;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofNamedElement;
import com._4meonweb.uml.deployments.DeploymentTarget;

/** Resulting Deployment Target merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofDeploymentTarget
    extends DeploymentTarget, EmofNamedElement {

}
