package com._4meonweb.mof.emof.api.uml.classification;

import com._4meonweb.mof.emof.api.uml.classifier.EmofClassifier;
import com._4meonweb.uml.classification.BehavioredClassifier;

/** Resulting EMOF Behaviored Classifier merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofBehavioredClassifier
    extends BehavioredClassifier, EmofClassifier {

}
