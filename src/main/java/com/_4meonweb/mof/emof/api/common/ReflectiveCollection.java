package com._4meonweb.mof.emof.api.common;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlInteger;

public interface ReflectiveCollection<O extends EmofObject> extends EmofObject {
  /** Adds object to the last position in the collection.
   *
   * @param object
   *          the new collection member
   * @return true if the object was added */
  default UmlBoolean add(final O object) {
    return UmlBooleanStatic.FALSE;
  }

  /** Adds the objects to the end of the collection.
   *
   * @param objects
   *          the new collection members
   * @return true if any elements were added */
  default UmlBoolean addAll(final ReflectiveCollection<O> objects) {
    return UmlBooleanStatic.FALSE;
  }

  /** Removes all objects from the collection. */
  default void clear() {
  }

  /** Removes the specified object from the collection.
   *
   * @param object
   *          the collection member
   * @return true if the object was removed */
  default UmlBoolean remove(final O object) {
    return UmlBooleanStatic.FALSE;
  }

  /** Gets collection size.
   *
   * @return the number of objects in the collection */
  default UmlInteger size() {
    return null;
  }
}
