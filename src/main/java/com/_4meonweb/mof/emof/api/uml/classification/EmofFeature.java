package com._4meonweb.mof.emof.api.uml.classification;

import com._4meonweb.uml.classification.Feature;

/** Resulting EMOF Feature merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofFeature extends Feature {

}
