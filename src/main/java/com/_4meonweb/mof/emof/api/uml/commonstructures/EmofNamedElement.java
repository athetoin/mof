package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.NamedElement;
import com._4meonweb.uml.commonstructures.VisibilityKind;

/** Resulting EMOF Named Element merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofNamedElement extends NamedElement, EmofElement {

  /** EMOF Named Element builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>
   *          the implemented element type
   * @param <M>the
   *          implemented builder type */
  public interface EmofNamedElementMaker<E extends EmofNamedElement,
      M extends EmofNamedElementMaker<E, M>> extends EmofElementMaker<E, M> {

    /** Puts name.
     *
     * @param name
     *          the name
     * @return this builder */
    default M putName(final UmlString name) {
      return null;
    }

    /** Puts Visibility.
     *
     * @param visibility
     *          the visibility
     * @return this builder */
    default M putVisibility(final VisibilityKind visibility) {
      return null;
    }
  }
}
