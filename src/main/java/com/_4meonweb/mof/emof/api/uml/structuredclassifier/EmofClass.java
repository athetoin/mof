package com._4meonweb.mof.emof.api.uml.structuredclassifier;

import com._4meonweb.mof.emof.api.uml.classification.EmofBehavioredClassifier;
import com._4meonweb.uml.structuredclassifier.UmlClass;

/** Resulting EMOF Class merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofClass
    extends UmlClass, EmofBehavioredClassifier, EmofEncapsulatedClassifier {

}
