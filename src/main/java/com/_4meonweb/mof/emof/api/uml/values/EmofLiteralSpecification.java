package com._4meonweb.mof.emof.api.uml.values;

import com._4meonweb.uml.values.LiteralSpecification;

/** Resulting EMOF Literal Specification merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofLiteralSpecification
    extends LiteralSpecification, EmofValueSpecification {

  /** EMOF Literal Specification builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>
   *          the implemented specification type
   * @param <M>the
   *          implemented builder type */
  public interface EmofLiteralSpecificationMaker<
      E extends EmofLiteralSpecification,
      M extends EmofLiteralSpecificationMaker<E, M>>
      extends EmofValueSpecificationMaker<E, M> {

  }
}
