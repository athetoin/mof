package com._4meonweb.mof.emof.api.uml.simpleclassifiers;

import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

/** Resulting EMOF Primitive Type merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofPrimitiveType extends PrimitiveType, EmofDataType {
  public interface EmofPrimitiveTypeMaker
      extends EmofDataTypeMaker<EmofPrimitiveType, EmofPrimitiveTypeMaker> {

  }
}
