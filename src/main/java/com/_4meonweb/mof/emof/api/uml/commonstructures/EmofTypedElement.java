package com._4meonweb.mof.emof.api.uml.commonstructures;

import com._4meonweb.uml.commonstructures.TypedElement;

/** Resulting EMOF TypedElement merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofTypedElement extends TypedElement, EmofNamedElement {

  /** EMOF type element builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>the
   *          implemented specification type
   * @param <M>the
   *          implemented builder type */
  public interface EmofTypedElementMaker<E extends EmofTypedElement,
      M extends EmofTypedElementMaker<E, M>>
      extends TypedElementMaker<E, M>, EmofNamedElementMaker<E, M> {

  }
}
