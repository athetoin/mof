package com._4meonweb.mof.emof.api.uml.deployments;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofNamedElement;
import com._4meonweb.uml.deployments.DeployedArtifact;

/** Resulting EMOF Deployed Artifact merged from UML.
 *
 * @author Maxim Rodyushkin */
public interface EmofDeployedArtifact
    extends DeployedArtifact, EmofNamedElement {

}
