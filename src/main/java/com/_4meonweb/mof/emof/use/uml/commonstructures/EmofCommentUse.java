package com._4meonweb.mof.emof.use.uml.commonstructures;

import com._4meonweb.mof.emof.api.uml.commonstructures.EmofComment;
import com._4meonweb.mof.emof.use.EmofObjectUse;
import com._4meonweb.uml.commonstructures.Element;

import java.util.Optional;

public final class EmofCommentUse extends EmofObjectUse implements EmofComment {
  private static final long serialVersionUID = -2511002307515851254L;

  public static class EmofCommentMakerUse
      extends EmofObjectMakerUse<EmofComment, EmofCommentMaker>
      implements EmofCommentMaker {
    private static final long serialVersionUID = -6787051478242000488L;

    @Override
    protected EmofCommentUse createObject() {
      return new EmofCommentUse();
    }

    @Override
    public EmofCommentMakerUse getItself() {
      return this;
    }

  }

  public EmofCommentUse() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public Optional<Element> getOwner() {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

}
