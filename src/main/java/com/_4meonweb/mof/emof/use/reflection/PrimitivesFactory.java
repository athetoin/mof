package com._4meonweb.mof.emof.use.reflection;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.reflection.EmofFactory;
import com._4meonweb.mof.emof.api.uml.commonstructures.EmofElement;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralBoolean;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralBoolean.EmofLiteralBooleanMaker;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralSpecification;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralString;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralString.EmofLiteralStringMaker;
import com._4meonweb.mof.emof.api.uml.values.LiteralStatic;
import com._4meonweb.mof.emof.use.EmofObjectUse;
import com._4meonweb.primitivetypes.UmlBoolean.UmlBooleanMaker;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.simpleclassifiers.DataType;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;
import com._4meonweb.uml.structuredclassifier.UmlClass;

import java.io.Serializable;
import java.util.Optional;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;

/** Static factories defined by EMOF.
 *
 * @author Maxim Rodyushkin */
public class PrimitivesFactory extends EmofObjectUse
    implements EmofFactory, Serializable {
  private static final long serialVersionUID = 1103485464475214598L;

  /** Boolean maker. */
  @Inject
  private transient UmlBooleanMaker blnMkr;

  @Inject
  @Named("Boolean")
  private transient Instance<PrimitiveType> blnTp;

  /** Literal Boolean maker factory. */
  @Inject
  private transient Instance<EmofLiteralBooleanMaker> ltrlBlnMkrFctr;

  /** Literal String maker factory. */
  @Inject
  private transient Instance<EmofLiteralStringMaker> ltrlStrMkrFctr;

  @Inject
  @Named("PrimitiveTypes")
  private transient UmlPackage prmvPckg;

  /** String factory. */
  @Inject
  private transient UmlStringMaker strMkr;

  @Inject
  @Named("String")
  private transient Instance<PrimitiveType> strTp;

  protected PrimitivesFactory() {
    super();
  }

  private void checkDataType(final DataType dataType) {
    if (dataType == null) {
      throw new NullPointerException("Data type is null");
    }
    if (this.getPackage().getPackagedElements()
        .filter(DataType.class::isInstance).map(DataType.class::cast)
        .noneMatch(dataType::equals)) {
      throw new IllegalArgumentException("Data type is not found");
    }
  }

  @Override
  public UmlString convertToString(final DataType dataType,
      final EmofObject object) {
    this.checkDataType(dataType);
    if (this.blnTp.get().equals(dataType)) {
      final boolean val = ((EmofLiteralBoolean) object).getValue().getValue();
      if (val) {
        return () -> "true";
      } else {
        return () -> "false";
      }
    } else {
      // String literal case
      return ((EmofLiteralString) object).getValue().orElseThrow(
          () -> new IllegalArgumentException("String literal has no value"));
    }
  }

  @Override
  public Optional<EmofElement> create(final UmlClass metaClass) {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

  @Override
  public EmofLiteralSpecification createFromString(final DataType dataType,
      final UmlString string) {
    this.checkDataType(dataType);
    if (this.blnTp.get().equals(dataType)) {
      final var val = this.stringToBoolean(string.getValue());
      if (val.isPresent()) {
        return this.ltrlBlnMkrFctr.get().putValue(this.blnMkr.ofThe(val.get()))
            .make().map(EmofLiteralSpecification.class::cast)
            .orElse(LiteralStatic.NULL);
      }
    } else if (this.strTp.get().equals(dataType)) {
      final var val = this.stringToString(string.getValue());
      if (val.isPresent()) {
        return this.ltrlStrMkrFctr.get().putValue(this.strMkr.ofThe(val.get()))
            .make().map(EmofLiteralSpecification.class::cast)
            .orElse(LiteralStatic.NULL);
      }
    }
    return LiteralStatic.NULL;
  }

  @Override
  public UmlPackage getPackage() {
    return this.prmvPckg;
  }

  private Optional<Boolean> stringToBoolean(final String string) {
    final var trmd = Optional.of(string.trim());
    final var trTst = trmd.filter(val -> "true".equals(val) || "1".equals(val))
        .map(str -> true);
    final var flsTst =
        trmd.filter(val -> "false".equals(val) || "0".equals(val))
            .map(str -> false);
    return trTst.isPresent() ? trTst : flsTst;
  }

  private Optional<String> stringToString(final String value) {
    return Optional.ofNullable(value.chars()
        .filter(cd -> cd >= 0x20 && cd <= 0xd7ff || cd == 0x9 || cd == 0xA
            || cd == 0xD || cd >= 0xe000 && cd <= 0xfffd
            || cd >= 0x10000 && cd <= 0x10ffff)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint,
            StringBuilder::append)
        .toString());
    // .filter(chr-> chr.)
    // #x9 | #xA | #xD |
    // [#x20-#xD7FF] |
    // [#xE000-#xFFFD] |
    // [#x10000-#x10FFFF]

  }

}
