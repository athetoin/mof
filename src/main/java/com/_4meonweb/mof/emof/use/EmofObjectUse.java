package com._4meonweb.mof.emof.use;

import com._4meonweb.mof.emof.api.EmofObject;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralBoolean;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralSpecification;
import com._4meonweb.mof.emof.api.uml.values.EmofLiteralString;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBoolean.UmlBooleanMaker;
import com._4meonweb.uml.structuredclassifier.Property;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

public class EmofObjectUse implements EmofObject, Serializable {
  private static final long serialVersionUID = -3525552801939642957L;

  @Inject
  protected transient Instance<UmlBooleanMaker> blnMkrFctr;

  protected transient Object object;

  protected transient ConcurrentMap<Property, Object> propertyValues =
      new ConcurrentHashMap<>();

  public abstract static class EmofObjectMakerUse<E extends EmofObject,
      M extends EmofObjectMaker<E, M>> extends EmofObjectUse
      implements EmofObjectMaker<E, M> {
    private static final long serialVersionUID = 4862446243993887948L;

    protected EmofObjectMakerUse() {
      super();
    }

    /** Creates new instance of maker.
     *
     * @return the new instance */
    protected abstract E createObject();

    @Override
    public Optional<E> make() {
      final E obj = this.createObject();
      ((EmofObjectUse) obj)
          .setPropertyValues(new ConcurrentHashMap<>(this.getPropertyValues()));
      return Optional.of(obj);
    }

    @Override
    public EmofObject ofThe(final Object obj) {
      final var newObj = new EmofObjectUse();
      newObj.setObject(obj);
      return newObj;

    }

    @Override
    public M set(final Property property, final EmofObject object) {
      this.getPropertyValues().put(property, object);
      return this.getItself();
    }
  }

  protected EmofObjectUse() {
    super();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof EmofObjectUse && this.object != null) {
      final EmofObjectUse other = (EmofObjectUse) obj;
      return Objects.equals(this.object, other.object);
    }
    if (obj instanceof EmofObject) {
      final var other = (EmofObject) obj;
      return this.objectEquals(other).getValue();
    }
    return false;
  }

  @Override
  public Optional<EmofObject> get(final Property property) {
    return Optional
        .ofNullable((EmofObject) this.getPropertyValues().get(property));
  }

  @Override
  public Object getObject() {
    return this.object == null ? this : this.object;
  }

  protected ConcurrentMap<Property, Object> getPropertyValues() {
    return this.propertyValues;
  }

  @Override
  public int hashCode() {
    Stream<Optional<? extends Object>> val =
        Stream.of(Optional.ofNullable(this.object));
    if (this instanceof EmofLiteralSpecification) {
      final var ltrl = (EmofLiteralSpecification) this;
      val = Stream.concat(val,
          Stream.of(ltrl.getBooleanValue(), ltrl.getIntegerValue(),
              ltrl.getRealValue(), ltrl.getStringValue(),
              ltrl.getUnlimitedValue()))
          .filter(Optional::isPresent);
    }
    final Optional<Integer> val0 = val.map(Optional::stream).flatMap(v -> v)
        .findFirst().map(Objects::hash);
    return val0.orElse(super.hashCode());
  }

  @Override
  public UmlBoolean objectEquals(final EmofObject object) {
    if (this instanceof EmofLiteralString
        && object instanceof EmofLiteralString) {
      final var v0 = ((EmofLiteralString) this).getValue();
      final var v1 = ((EmofLiteralString) object).getValue();
      return this.blnMkrFctr.get().ofThe(v0.equals(v1));
    }
    if (this instanceof EmofLiteralBoolean
        && object instanceof EmofLiteralBoolean) {
      final var v0 = ((EmofLiteralBoolean) this).getValue();
      final var v1 = ((EmofLiteralBoolean) object).getValue();
      return this.blnMkrFctr.get().ofThe(v0.equals(v1));
    }
    return EmofObject.super.objectEquals(object);
  }

  /** Sets underlying object.
   *
   * @param object
   *          the object to set */
  protected void setObject(final Object object) {
    this.object = object;
  }

  protected void setPropertyValues(
      final ConcurrentMap<Property, Object> values) {
    this.propertyValues = values;
  }
}
