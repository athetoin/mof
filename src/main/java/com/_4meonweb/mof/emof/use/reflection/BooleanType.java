package com._4meonweb.mof.emof.use.reflection;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.uml.commonstructures.Comment;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Named("Boolean")
public class BooleanType implements PrimitiveType, Serializable {
  private static final long serialVersionUID = 1687289175638221064L;

  final transient Comment cmnt = new BooleanComment();

  @Inject
  @Named("PrimitiveTypes")
  transient Instance<UmlPackage> prmvPckg;

  @Inject
  transient UmlStringMaker strMkr;

  public class BooleanComment implements Comment {
    @Override
    public Stream<Element> getAnnotatedElements() {
      return Stream.of(BooleanType.this);
    }

    @Override
    public Optional<UmlString> getBody() {
      return Optional.of(() -> "Boolean is used for logical expressions, "
          + "consisting of the predefined values true and false.");
    }

    @Override
    public Optional<Element> getOwner() {
      return Optional.of(BooleanType.this);
    }

    @Override
    public String toString() {
      return "Description of Boolean";
    }
  }

  protected BooleanType() {
    super();
  }

  @Override
  public Optional<UmlString> getName() {
    return Optional.of(this.strMkr.ofThe(this.toString()));
  }

  @Override
  public Optional<Namespace> getNamespace() {
    return Optional.ofNullable(this.prmvPckg.get());
  }

  @Override
  public Stream<Comment> getOwnedComments() {
    return Stream.of(this.cmnt);
  }

  @Override
  public String toString() {
    return "Boolean";
  }
}
