package com._4meonweb.mof.emof.use.reflection;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.uml.commonstructures.PackageableElement;
import com._4meonweb.uml.commonstructures.UmlPackage;
import com._4meonweb.uml.simpleclassifiers.PrimitiveType;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Named("PrimitiveTypes")
public class PrimitiveTypePackage implements UmlPackage, Serializable {
  private static final long serialVersionUID = 3835822709163638964L;

  @Inject
  @Named("Boolean")
  public transient Instance<PrimitiveType> bln;

  @Inject
  @Named("Integer")
  public transient Instance<PrimitiveType> intrTp;

  @Inject
  transient UmlStringMaker strMkr;

  @Inject
  @Named("String")
  public transient Instance<PrimitiveType> strTp;

  @Override
  public Optional<UmlString> getName() {
    return Optional.of(this.strMkr.ofThe(this.toString()));
  }

  @Override
  public Stream<PackageableElement> getPackagedElements() {
    return Stream.of(this.bln.get(), this.intrTp.get(), this.strTp
        .get()/* ,EmofPrimitives.REAL, EmofPrimitives.UNLIMITED_NATURAL */);
  }

  UmlPackage getPrimitiveTypes() {
    return this;
  }

  @Override
  public Optional<UmlString> getUri() {
    return Optional.of(
        this.strMkr.ofThe("http://www.omg.org/spec/PrimitiveTypes/20161101"));
  }

  @Override
  public String toString() {
    return "PrimitiveTypes";
  }
}
