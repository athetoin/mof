package com._4meonweb.mof.emof.use.uml.values;

import com._4meonweb.mof.emof.api.uml.values.EmofLiteralBoolean;
import com._4meonweb.mof.emof.use.EmofObjectUse;
import com._4meonweb.uml.commonstructures.Element;

import java.util.Optional;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

public class EmofLiteralBooleanUse extends EmofObjectUse
    implements EmofLiteralBoolean {
  private static final long serialVersionUID = 5148879751916005987L;

  public static class EmofLiteralBooleanMakerUse
      extends EmofObjectMakerUse<EmofLiteralBoolean, EmofLiteralBooleanMaker>
      implements EmofLiteralBooleanMaker {
    private static final long serialVersionUID = 8380440930535159015L;

    @Inject
    private transient Instance<EmofLiteralBoolean> objFctr;

    protected EmofLiteralBooleanMakerUse() {
      super();
    }

    @Override
    protected EmofLiteralBoolean createObject() {
      return this.objFctr.get();
    }

    @Override
    public EmofLiteralBooleanMaker getItself() {
      return this;
    }

    @Override
    public Optional<Element> getOwner() {
      // TODO Auto-generated method stub
      return Optional.empty();
    }

  }

  protected EmofLiteralBooleanUse() {
    super();
  }

  @Override
  public Optional<Element> getOwner() {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

}
