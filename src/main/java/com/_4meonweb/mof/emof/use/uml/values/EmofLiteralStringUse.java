package com._4meonweb.mof.emof.use.uml.values;

import com._4meonweb.mof.emof.api.uml.values.EmofLiteralString;
import com._4meonweb.mof.emof.use.EmofObjectUse;
import com._4meonweb.uml.commonstructures.Element;

import java.util.Optional;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

public class EmofLiteralStringUse extends EmofObjectUse
    implements EmofLiteralString {
  private static final long serialVersionUID = -59777927629876943L;

  public static class EmofLiteralStringMakerUse
      extends EmofObjectMakerUse<EmofLiteralString, EmofLiteralStringMaker>
      implements EmofLiteralStringMaker {
    private static final long serialVersionUID = -4505895617462829466L;

    @Inject
    private transient Instance<EmofLiteralString> objFctr;

    protected EmofLiteralStringMakerUse() {
      super();
    }

    @Override
    protected EmofLiteralString createObject() {
      return this.objFctr.get();
    }

    @Override
    public EmofLiteralStringMaker getItself() {
      return this;
    }

    @Override
    public Optional<Element> getOwner() {
      // TODO Auto-generated method stub
      return Optional.empty();
    }
  }

  protected EmofLiteralStringUse() {
    super();
  }

  @Override
  public Optional<Element> getOwner() {
    // TODO Auto-generated method stub
    return Optional.empty();
  }

}
