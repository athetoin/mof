package com._4meonweb.mof.cmof;

import com._4meonweb.mof.emof.api.EmofObject;

/** This is a new data type that is used to represent named arguments to
 * open-ended reflective operations. It is open-ended and allows both Elements
 * and data values to be supplied.
 *
 * @author Maxim Rodyushkin */
public interface Argument {
  /** The name of the argument.
   *
   * @return the name */
  String getName();

  /** The value of the argument.
   *
   * @return the value */
  EmofObject getValue();
}
